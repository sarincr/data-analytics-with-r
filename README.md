# Data-Analytics-with-R


R analytics (or R programming language) is a free, open-source software used for all kinds of data science, statistics, and visualization projects. R programming language is powerful, versatile, AND able to be integrated into BI platforms. It is mainly used by statisticians and data miners for developing statistical software and performing data analysis.

This article covers the steps required to install R on Ubuntu.

```
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran40/'
sudo apt update
sudo apt install r-base
sudo -i R
```
